﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common
{
    public class HateoasRestResult<T>
    {
        [JsonProperty("_embedded")]
        public HateoasEmbedded<T> Embedded { get; set; }
        [JsonProperty("page")]
        public PageRestResult Page { get; set; }

        public override string ToString()
        {
            string result = "{" +
                            "hasNext=" + Page.HasNext +
                            ", hasPrev=" + Page.HasPrev +
                            ", items=[";
            if (Embedded.Items != null && Embedded.Items.Length > 0)
            {
                for (int i = 0; i < Embedded.Items.Length; i++)
                {
                    if (i > 0)
                    {
                        result += ", ";
                    }
                    result += Embedded.Items[i];
                }
            }
            result = result + "]}";

            return result;
        }
    }
}

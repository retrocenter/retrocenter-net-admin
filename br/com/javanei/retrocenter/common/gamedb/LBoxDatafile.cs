﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.gamedb
{
    public class LBoxDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
    }
}

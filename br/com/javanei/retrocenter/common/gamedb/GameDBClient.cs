﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.gamedb
{
    public class GameDBClient
    {
        static Config config = new Config();

        public static RestResult<LBoxDatafile> GetLBoxDatafiles(int page = 0, int pageSize = 100)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<LBoxDatafile> result = new RestResult<LBoxDatafile>();

                string uri = config.ServerContextPath + "/api/gamedb/launchbox/?page="
                    + page + "&pageSize=" + pageSize;

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<LBoxDatafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<LBoxDatafile> GetAllLBoxDatafiles()
        {
            LinkedList<LBoxDatafile> result = new LinkedList<LBoxDatafile>();

            int page = 0;

            RestResult<LBoxDatafile> p = GetLBoxDatafiles(page: 0);
            while (p != null)
            {
                foreach (LBoxDatafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetLBoxDatafiles(page: page);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }
    }
}

﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common
{
    public class PageRestResult
    {
        [JsonProperty("hasNext")]
        public bool HasNext { get; set; } = false;
        [JsonProperty("hasPrev")]
        public bool HasPrev { get; set; } = false;
    }
}

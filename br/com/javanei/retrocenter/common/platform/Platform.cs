﻿namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.platform
{
    public class Platform
    {
        public long id { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string storageFolder { get; set; }
        public string[] alternateNames { get; set; } = new string[0];

        public string AlternateNamesAsString
        {
            get
            {
                string result = "";
                if (alternateNames != null && alternateNames.Length > 0)
                {
                    for (int i = 0; i < alternateNames.Length; i++)
                    {
                        if (i > 0)
                        {
                            result += "\n";
                        }
                        result += alternateNames[i];
                    }
                }
                return result;
            }
        }

        public override string ToString()
        {
            string result = "{"
                + "\"id\": " + id + ""
                + ", \"name\": \"" + name + "\""
                + ", \"shortName\": \"" + shortName + "\"";
            if (storageFolder != null)
            {
                result += ", \"storageFolder\": \"" + storageFolder + "\"";
            }
            if (alternateNames != null && alternateNames.Length > 0)
            {
                result = result + ", \"alternateNames\": [";
                for (int i = 0; i < alternateNames.Length; i++)
                {
                    if (i > 0)
                    {
                        result += ", ";
                    }
                    result += "\"" + alternateNames[i] + "\"";
                }
                result += "]";
            }
            result += "}";
            return result;
        }
    }
}

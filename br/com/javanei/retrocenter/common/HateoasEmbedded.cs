﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common
{
    public class HateoasEmbedded<T>
    {
        [JsonProperty("items")]
        public T[] Items { get; set; } = new T[0];
    }
}

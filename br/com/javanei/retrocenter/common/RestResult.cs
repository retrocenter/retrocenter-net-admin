﻿namespace RetrocenterAdmin.br.com.javanei.retrocenter.common
{
    public class RestResult<T>
    {
        public bool hasNext { get; set; } = false;
        public bool hasPrev { get; set; } = false;
        public T[] items { get; set; } = new T[0];

        public override string ToString()
        {
            string result = "{" +
                    "hasNext=" + hasNext +
                    ", hasPrev=" + hasPrev +
                    ", items=[";
            if (items != null && items.Length > 0)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    if (i > 0)
                    {
                        result += ", ";
                    }
                    result += items[i];
                }
            }
            result = result + "]}";

            return result;
        }
    }
}

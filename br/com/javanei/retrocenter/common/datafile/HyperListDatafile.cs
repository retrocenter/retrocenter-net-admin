﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class HyperListDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("header")]
        public HyperListHeader Header { get; set; }
    }
}

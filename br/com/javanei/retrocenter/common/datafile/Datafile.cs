﻿namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class Datafile
    {
        public long id { get; set; }
        public string name { get; set; }
        public string catalog { get; set; }
        public string version { get; set; }
        public string description { get; set; }
        public string author { get; set; }
        public string date { get; set; }
        public string email { get; set; }
        public string homepage { get; set; }
        public string url { get; set; }
        public string comment { get; set; }
        public string platformName { get; set; }
        //private Set<DatafileArtifact> artifacts = new HashSet<>();
    }
}

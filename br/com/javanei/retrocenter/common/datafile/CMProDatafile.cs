﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class CMProDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("header")]
        public CMProHeader Header { get; set; }
    }
}

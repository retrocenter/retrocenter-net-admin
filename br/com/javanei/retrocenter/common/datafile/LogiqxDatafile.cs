﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class LogiqxDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("build")]
        public string Build { get; set; }
        [JsonProperty("debug")]
        public string Debug { get; set; }
        [JsonProperty("header")]
        public LogiqxHeader Header { get; set; }
    }
}

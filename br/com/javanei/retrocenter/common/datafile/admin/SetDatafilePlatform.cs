﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin
{
    public class SetDatafilePlatform
    {
        [JsonProperty("audited")]
        public bool Audited = false;
        [JsonProperty("platformId")]
        public long PlatformId = -1;
    }
}

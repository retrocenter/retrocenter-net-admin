﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin
{
    public class AdminDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; } = 0;
        [JsonProperty("name")]
        public string Name { get; set; } = "";
        [JsonProperty("catalog")]
        public string Catalog { get; set; } = "";
        [JsonProperty("version")]
        public string Version { get; set; } = "";
        [JsonProperty("platformAudited")]
        public bool PlatformAudited { get; set; } = false;
        [JsonProperty("platformId")]
        public long PlatformId { get; set; } = 0;
        [JsonProperty("platformName")]
        public string PlatformName { get; set; } = "";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin
{
    public class AdminDatafileArtifact
    {
        [JsonProperty("datafileArtifactId")]
        public long DatafileArtifactId { get; set; } = 0;

        [JsonProperty("name")]
        public string Name { get; set; } = "";

        [JsonProperty("description")]
        public string Description { get; set; } = "";

        [JsonProperty("year")]
        public string Year { get; set; } = "";

        [JsonProperty("comment")]
        public string Comment { get; set; } = "";

        [JsonProperty("datafileId")]
        public long DatafileId { get; set; } = 0;

        [JsonProperty("audited")]
        public bool Audited { get; set; } = false;

        /*
        private Map<String, String> fields = new HashMap<>();
        private AdminDatafileArtifactAuditedDTO auditedInfo = null;
        */
    }
}

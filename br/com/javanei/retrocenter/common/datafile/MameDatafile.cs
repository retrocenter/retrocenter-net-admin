﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class MameDatafile
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("build")]
        public string Build { get; set; }
        [JsonProperty("debug")]
        public string Debug { get; set; }
        [JsonProperty("mameconfig")]
        public string Mameconfig { get; set; }
    }
}

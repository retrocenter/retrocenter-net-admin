﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class HyperListHeader
    {
        [JsonProperty("listname")]
        public string Listname { get; set; }
        [JsonProperty("lastlistupdate")]
        public string Lastlistupdate { get; set; }
        [JsonProperty("listversion")]
        public string Listversion { get; set; }
        [JsonProperty("exporterversion")]
        public string Exporterversion { get; set; }
    }
}

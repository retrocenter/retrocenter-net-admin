﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class AdminDatafileClient
    {
        static readonly Config config = new Config();

        public static AdminDatafile GetDatafileById(long datafileId)
        {
            AdminDatafile result = null;

            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/hal+json"));
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string uri = config.ServerContextPath + "/admin/api/datafiles/" + datafileId;

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    string s = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                        s = response.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AdminDatafile>(s);
                    }
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static AdminDatafile SetDatafilePlatform(long datafileId, SetDatafilePlatform data)
        {
            AdminDatafile result = null;

            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/hal+json"));
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string uri = config.ServerContextPath + "/admin/api/datafiles/" + datafileId + "/platform";

                HttpContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(uri, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    string s = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<AdminDatafile>(s);
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static AdminDatafileArtifact[] GetArtifactsOfDatafile(long datafileId)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/hal+json"));

                AdminDatafileArtifact[] result = null;

                string uri = config.ServerContextPath + "/admin/api/datafiles/" + datafileId + "/artifacts";

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    string s = response.Content.ReadAsStringAsync().Result;
                    HateoasRestResult<AdminDatafileArtifact> hateoas =
                        JsonConvert.DeserializeObject<HateoasRestResult<AdminDatafileArtifact>>(s);
                    if (hateoas?.Embedded?.Items != null)
                    {
                        result = hateoas.Embedded.Items;
                    }
                    else
                    {
                        result = null;
                    }
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static HateoasRestResult<AdminDatafile> GetDatafiles(int page = 0, int pageSize = 100,
            string sort = null, string audited = "all", string name = null, string catalog = null, string platform = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/hal+json"));

                HateoasRestResult<AdminDatafile> result = new HateoasRestResult<AdminDatafile>();

                string uri = config.ServerContextPath + "/admin/api/datafiles/?page="
                                                      + page + "&pageSize=" + pageSize;
                if (sort != null && sort.Length > 0)
                {
                    uri += "&sort=" + HttpUtility.UrlEncode(sort);
                }
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }
                if (catalog != null && catalog.Length > 0)
                {
                    uri += "&catalog=" + HttpUtility.UrlEncode(catalog);
                }
                if (platform != null && platform.Length > 0)
                {
                    uri += "&platform=" + HttpUtility.UrlEncode(platform);
                }

                if (audited != null && audited.Length > 0)
                {
                    uri += "&audited=" + HttpUtility.UrlEncode(audited);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    //Nao funciona result = response.Content.ReadAsAsync<HateoasRestResult<AdminDatafile>>().Result;
                    string s = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<HateoasRestResult<AdminDatafile>>(s);
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<AdminDatafile> GetAllDatafiles(string auditedStatus = "all", string catalogName = null)
        {
            LinkedList<AdminDatafile> result = new LinkedList<AdminDatafile>();

            int page = 0;

            HateoasRestResult<AdminDatafile> p = GetDatafiles(page: 0, catalog: catalogName, audited: auditedStatus);
            while (p != null && p.Embedded != null && p.Embedded.Items != null && p.Embedded.Items.Length > 0)
            {
                foreach (AdminDatafile datafile in p.Embedded.Items)
                {
                    result.AddLast(datafile);
                }
                if (p.Page.HasNext)
                {
                    page++;
                    p = GetDatafiles(page: page, catalog: catalogName, audited: auditedStatus);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }
    }
}

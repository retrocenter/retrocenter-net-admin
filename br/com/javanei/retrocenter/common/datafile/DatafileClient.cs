﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class DatafileClient
    {
        static Config config = new Config();

        public static List<string> Catalogs = new List<string>()
        {
            "",
            "MAME",
            "NoIntro",
            "TOSEC",
            "GoodSet",
            "HyperList"
        };

        public static RestResult<Datafile> GetDatafiles(int page = 0, int pageSize = 100,
            string sort = null, string name = null, string catalog = null, string platform = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<Datafile> result = new RestResult<Datafile>();

                string uri = config.ServerContextPath + "/api/datafiles/?page="
                    + page + "&pageSize=" + pageSize;
                if (sort != null && sort.Length > 0)
                {
                    uri += "&sort=" + HttpUtility.UrlEncode(sort);
                }
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }
                if (catalog != null && catalog.Length > 0)
                {
                    uri += "&catalog=" + HttpUtility.UrlEncode(catalog);
                }
                if (platform != null && platform.Length > 0)
                {
                    uri += "&platform=" + HttpUtility.UrlEncode(platform);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<Datafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<Datafile> GetAllDatafiles(string platformName = null, string catalogName = null)
        {
            LinkedList<Datafile> result = new LinkedList<Datafile>();

            int page = 0;

            RestResult<Datafile> p = GetDatafiles(page: 0, catalog: catalogName, platform: platformName);
            while (p != null)
            {
                foreach (Datafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetDatafiles(page: page, catalog: catalogName, platform: platformName);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }

        public static RestResult<CMProDatafile> GetCMProDatafiles(int page = 0, int pageSize = 100, string name = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<CMProDatafile> result = new RestResult<CMProDatafile>();

                string uri = config.ServerContextPath + "/api/datafiles/cmpro/?page="
                    + page + "&pageSize=" + pageSize;
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<CMProDatafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<CMProDatafile> GetAllCMProDatafiles()
        {
            LinkedList<CMProDatafile> result = new LinkedList<CMProDatafile>();

            int page = 0;

            RestResult<CMProDatafile> p = GetCMProDatafiles(page: 0);
            while (p != null)
            {
                foreach (CMProDatafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetCMProDatafiles(page: page);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }

        public static RestResult<LogiqxDatafile> GetLogiqxDatafiles(int page = 0, int pageSize = 100, string name = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<LogiqxDatafile> result = new RestResult<LogiqxDatafile>();

                string uri = config.ServerContextPath + "/api/datafiles/logiqx/?page="
                    + page + "&pageSize=" + pageSize;
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<LogiqxDatafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<LogiqxDatafile> GetAllLogiqxDatafiles()
        {
            LinkedList<LogiqxDatafile> result = new LinkedList<LogiqxDatafile>();

            int page = 0;

            RestResult<LogiqxDatafile> p = GetLogiqxDatafiles(page: 0);
            while (p != null)
            {
                foreach (LogiqxDatafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetLogiqxDatafiles(page: page);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }

        public static RestResult<HyperListDatafile> GetHyperListDatafiles(int page = 0, int pageSize = 100, string name = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<HyperListDatafile> result = new RestResult<HyperListDatafile>();

                string uri = config.ServerContextPath + "/api/datafiles/hyperlist/?page="
                    + page + "&pageSize=" + pageSize;
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<HyperListDatafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<HyperListDatafile> GetAllHyperListDatafiles()
        {
            LinkedList<HyperListDatafile> result = new LinkedList<HyperListDatafile>();

            int page = 0;

            RestResult<HyperListDatafile> p = GetHyperListDatafiles(page: 0);
            while (p != null)
            {
                foreach (HyperListDatafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetHyperListDatafiles(page: page);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }

        public static RestResult<MameDatafile> GetMameDatafiles(int page = 0, int pageSize = 100, string name = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<MameDatafile> result = new RestResult<MameDatafile>();

                string uri = config.ServerContextPath + "/api/datafiles/mame/?page="
                    + page + "&pageSize=" + pageSize;
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<MameDatafile>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<MameDatafile> GetAllMameDatafiles()
        {
            LinkedList<MameDatafile> result = new LinkedList<MameDatafile>();

            int page = 0;

            RestResult<MameDatafile> p = GetMameDatafiles(page: 0);
            while (p != null)
            {
                foreach (MameDatafile datafile in p.items)
                {
                    result.AddLast(datafile);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetMameDatafiles(page: page);
                }
                else
                {
                    p = null;
                }
            }
            return result;
        }
    }
}

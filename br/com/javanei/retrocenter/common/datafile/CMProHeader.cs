﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class CMProHeader
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("catalog")]
        public string Catalog { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("author")]
        public string Author { get; set; }
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("forcemerging")]
        public string Forcemerging { get; set; }
        [JsonProperty("forcezipping")]
        public string Forcezipping { get; set; }
    }
}

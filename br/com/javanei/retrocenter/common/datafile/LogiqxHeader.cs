﻿using Newtonsoft.Json;


namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile
{
    public class LogiqxHeader
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("catalog")]
        public string Catalog { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("author")]
        public string Author { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("comment")]
        public string Comment { get; set; }
        [JsonProperty("header")]
        public string Header { get; set; }
        [JsonProperty("forcemerging")]
        public string Forcemerging { get; set; }
        [JsonProperty("forcenodump")]
        public string Forcenodump { get; set; }
        [JsonProperty("forcepacking")]
        public string Forcepacking { get; set; }
        [JsonProperty("plugin")]
        public string Plugin { get; set; }
        [JsonProperty("rommode")]
        public string Rommode { get; set; }
        [JsonProperty("biosmode")]
        public string Biosmode { get; set; }
        [JsonProperty("samplemode")]
        public string Samplemode { get; set; }
        [JsonProperty("lockrommode")]
        public string Lockrommode { get; set; }
        [JsonProperty("lockbiosmode")]
        public string Lockbiosmode { get; set; }
        [JsonProperty("locksamplemode")]
        public string Locksamplemode { get; set; }
    }
}

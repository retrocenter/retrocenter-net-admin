﻿using Newtonsoft.Json;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.common.artifact
{
    public class PlatformArtifactFileImportHistory
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("platformID")]
        public long PlatformID { get; set; }
        [JsonProperty("platformName")]
        public string PlatformName { get; set; }
    }
}

﻿using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin
{
    /// <summary>
    /// Interaction logic for OptionsPanel.xaml
    /// </summary>
    public partial class OptionsPanel : UserControl
    {
        private Config config = new Config();

        public OptionsPanel()
        {
            InitializeComponent();
        }


        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            config.ServerURL = ServerURL.Text;
            config.ServerContextPath = ServerContextPath.Text;
            config.Save();
            //Close();
        }

        /*
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            //Close();
        }
        */

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ServerURL.Text = config.ServerURL;
            ServerContextPath.Text = config.ServerContextPath;
        }
    }
}

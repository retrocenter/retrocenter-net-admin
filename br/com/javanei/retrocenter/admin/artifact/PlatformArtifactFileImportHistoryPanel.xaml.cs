﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform;
using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.artifact;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.platform;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.artifact
{
    /// <summary>
    /// Interaction logic for PlatformArtifactFileImportHistoryPanel.xaml
    /// </summary>
    public partial class PlatformArtifactFileImportHistoryPanel : UserControl
    {
        private ObservableCollection<PlatformArtifactFileImportHistory> History = new ObservableCollection<PlatformArtifactFileImportHistory>();
        private bool Ready = false;

        public PlatformArtifactFileImportHistoryPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadPlatform();
            DataGridHistory.ItemsSource = History;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                ComboboxItem p = (ComboboxItem)CBPlatform.SelectedItem;
                LinkedList<PlatformArtifactFileImportHistory> list =
                    PlatformArtifactFileClient.GetPlatformArtifactFileImportHistory(long.Parse(p.Value));
                History.Clear();
                foreach (PlatformArtifactFileImportHistory d in list)
                {
                    History.Add(d);
                }
            }
        }

        private void LoadPlatform()
        {
            ObservableCollection<ComboboxItem> items = new ObservableCollection<ComboboxItem>();
            LinkedList<Platform> platforms = PlatformClient.GetAllPlatforms();

            foreach (Platform p in platforms)
            {
                items.Add(new ComboboxItem(p.name, p.id.ToString()));
            }
            CBPlatform.ItemsSource = items;
        }

        private void CBPlatform_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }
    }
}

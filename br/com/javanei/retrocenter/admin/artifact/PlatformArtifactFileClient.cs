﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.artifact;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.artifact
{
    public class PlatformArtifactFileClient
    {
        static Config config = new Config();

        public static RestResult<PlatformArtifactFileImportHistory> GetArtifactFileImportHistory(
            long platformId, int page = 0, int pageSize = 100)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<PlatformArtifactFileImportHistory> result = new RestResult<PlatformArtifactFileImportHistory>();

                string uri = config.ServerContextPath + "/api/platforms/" + platformId + "/import-history?page="
                    + page + "&pageSize=" + pageSize + "&showFiles=false";

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<PlatformArtifactFileImportHistory>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<PlatformArtifactFileImportHistory> GetPlatformArtifactFileImportHistory(long platformId)
        {
            LinkedList<PlatformArtifactFileImportHistory> result = new LinkedList<PlatformArtifactFileImportHistory>();

            int page = 0;

            RestResult<PlatformArtifactFileImportHistory> p = GetArtifactFileImportHistory(platformId);
            while (p != null)
            {
                foreach (PlatformArtifactFileImportHistory file in p.items)
                {
                    result.AddLast(file);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetArtifactFileImportHistory(platformId);
                }
                else
                {
                    p = null;
                }
            }

            return result;
        }
    }
}

﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform;
using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.platform;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for DatafilesAllPanel.xaml
    /// </summary>
    public partial class DatafilesAllPanel : UserControl
    {
        private ObservableCollection<Datafile> Datafiles = new ObservableCollection<Datafile>();
        private bool Ready = false;

        public DatafilesAllPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadCatalog();
            LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                ComboboxItem p = (ComboboxItem)CBPlatform.SelectedItem;
                LinkedList<Datafile> list = DatafileClient.GetAllDatafiles(p.Value,
                    CBCatalog.SelectedItem.ToString());
                Datafiles.Clear();
                foreach (Datafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }

        private void LoadCatalog()
        {
            ObservableCollection<ComboboxItem> items = new ObservableCollection<ComboboxItem>();
            items.Clear();
            foreach (string s in DatafileClient.Catalogs)
            {
                items.Add(new ComboboxItem(s, s));
            }
            CBCatalog.ItemsSource = items;
        }

        private void LoadPlatform()
        {
            ObservableCollection<ComboboxItem> items = new ObservableCollection<ComboboxItem>();
            items.Add(new ComboboxItem("All", ""));
            items.Add(new ComboboxItem("Unknown", "null"));
            LinkedList<Platform> platforms = PlatformClient.GetAllPlatforms();

            foreach (Platform p in platforms)
            {
                items.Add(new ComboboxItem(p.name, p.name));
            }
            CBPlatform.ItemsSource = items;
        }

        private void CBCatalog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }

        private void CBPlatform_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }

        private void MenuItem_DatafileRowClick(object sender, RoutedEventArgs e)
        {
            Datafile datafile = (Datafile)DataGridDatafiles.SelectedItem;
            Console.WriteLine("@@@@@ " + datafile.id + "=" + datafile.name);
        }

        private void DataGridDatafiles_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Console.WriteLine("@@@@@@@ MouseDoubleClick");
        }
    }
}

﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform;
using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.platform;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for AuditDatafilesWindow.xaml
    /// </summary>
    public partial class AuditDatafilesWindow : Window
    {
        private AuditDatafilesView _context = new AuditDatafilesView();
        private LinkedList<AdminDatafile> _datafiles = new LinkedList<AdminDatafile>();

        private bool _ready = false;

        public AuditDatafilesWindow()
        {
            InitializeComponent();
            this.DataContext = _context;
            _context.Datafile = new AdminDatafile();
        }

        private void CBCatalog_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }

        private void CBAuditedFilter_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Filter();
        }

        private void AuditDatafilesWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadCatalog();
            LoadPlatform();

            _ready = true;

            Filter();
        }

        private void LoadCatalog()
        {
            List<ComboboxItem> items = new List<ComboboxItem>();
            items.Clear();
            foreach (string s in DatafileClient.Catalogs)
            {
                items.Add(new ComboboxItem(s, s));
            }
            _context.Catalogs = items;
            CBCatalog.SelectedIndex = 1;
        }

        private void LoadPlatform()
        {
            List<ComboboxItem> items = new List<ComboboxItem>();
            LinkedList<Platform> platforms = PlatformClient.GetAllPlatforms();

            foreach (Platform p in platforms)
            {
                items.Add(new ComboboxItem(p.name, p.id.ToString()));
            }
            _context.Platforms = items;
        }

        private void Filter()
        {
            if (_ready)
            {
                _context.LoadData(CBAuditedFilter.SelectedItem.ToString(),
                    CBCatalog.SelectedItem.ToString());
            }
        }

        private void BTSaveAndNext_Click(object sender, RoutedEventArgs e)
        {
            Save();
            _context.Next();
            CBPlatform.Focus();
        }

        private void BTSkip_Click(object sender, RoutedEventArgs e)
        {
            _context.Next();
            CBPlatform.Focus();
        }

        private void BTSkip10_Click(object sender, RoutedEventArgs e)
        {
            _context.Next10();
            CBPlatform.Focus();
        }

        private void BTPrev_Click(object sender, RoutedEventArgs e)
        {
            _context.Prev();
            CBPlatform.Focus();
        }

        private void BTSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void Save()
        {
            AdminDatafile datafile = _context.Datafile;
            if (datafile.PlatformId > 0)
            {
                SetDatafilePlatform data = new SetDatafilePlatform
                {
                    Audited = datafile.PlatformAudited,
                    PlatformId = datafile.PlatformId
                };
                AdminDatafileClient.SetDatafilePlatform(datafile.Id, data);
            }
        }
    }
}

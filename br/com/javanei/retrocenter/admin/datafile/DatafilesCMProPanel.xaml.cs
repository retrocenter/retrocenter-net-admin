﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for DatafilesCMProPanel.xaml
    /// </summary>
    public partial class DatafilesCMProPanel : UserControl
    {
        private ObservableCollection<CMProDatafile> Datafiles = new ObservableCollection<CMProDatafile>();
        private bool Ready = false;

        public DatafilesCMProPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadCatalog();
            //LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                LinkedList<CMProDatafile> list = DatafileClient.GetAllCMProDatafiles();
                Datafiles.Clear();
                foreach (CMProDatafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }
    }
}

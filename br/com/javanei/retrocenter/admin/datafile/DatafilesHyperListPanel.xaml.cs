﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for DatafilesHyperListPanel.xaml
    /// </summary>
    public partial class DatafilesHyperListPanel : UserControl
    {
        private ObservableCollection<HyperListDatafile> Datafiles = new ObservableCollection<HyperListDatafile>();
        private bool Ready = false;

        public DatafilesHyperListPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadCatalog();
            //LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                LinkedList<HyperListDatafile> list = DatafileClient.GetAllHyperListDatafiles();
                Datafiles.Clear();
                foreach (HyperListDatafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }
    }
}

﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for DatafilesMamePanel.xaml
    /// </summary>
    public partial class DatafilesMamePanel : UserControl
    {
        private ObservableCollection<MameDatafile> Datafiles = new ObservableCollection<MameDatafile>();
        private bool Ready = false;

        public DatafilesMamePanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadCatalog();
            //LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                LinkedList<MameDatafile> list = DatafileClient.GetAllMameDatafiles();
                Datafiles.Clear();
                foreach (MameDatafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }
    }
}

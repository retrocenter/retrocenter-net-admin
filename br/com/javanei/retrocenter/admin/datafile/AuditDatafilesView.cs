﻿using System;
using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile.admin;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    public class AuditDatafilesView : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _currentItem = -1;

        private AdminDatafile _datafile = new AdminDatafile();
        public AdminDatafile Datafile
        {
            get => _datafile;

            set
            {
                if (value != null)
                {
                    _datafile = value;
                    HasCurrent = true;
                    Artifacts = AdminDatafileClient.GetArtifactsOfDatafile(value.Id);
                }
                else
                {
                    this._datafile = new AdminDatafile();
                    HasCurrent = false;
                    Artifacts = new AdminDatafileArtifact[0];
                }
                OnPropertyChanged("Datafile");
            }
        }

        private List<ComboboxItem> _platforms = new List<ComboboxItem>();
        public List<ComboboxItem> Platforms
        {
            get => _platforms;
            set
            {
                this._platforms = value;
                OnPropertyChanged("Platforms");
            }
        }

        private List<ComboboxItem> _catalogs = new List<ComboboxItem>();
        public List<ComboboxItem> Catalogs
        {
            get => _catalogs;
            set
            {
                this._catalogs = value;
                OnPropertyChanged("Catalogs");
            }
        }

        private List<ComboboxItem> _auditedFilter = new List<ComboboxItem>()
        {
            new ComboboxItem("unaudited", "unaudited"),
            new ComboboxItem("all", "all"),
            new ComboboxItem("audited", "audited")
        };
        public List<ComboboxItem> AuditedFilter
        {
            get => _auditedFilter;
            set
            {
                this._auditedFilter = value;
                OnPropertyChanged("AuditedFilter");
            }
        }

        private AdminDatafile[] _datafiles = new AdminDatafile[0];
        public AdminDatafile[] Datafiles
        {
            get => _datafiles;
            set
            {
                if (value != null && value.Length > 0)
                {
                    _datafiles = value;
                    Datafile = _datafiles[0];
                    //Datafile = AdminDatafileClient.GetDatafileById(_datafiles[0].Id);
                    _currentItem = 0;
                    HasNext = _datafiles.Length > 1;
                    HasPrev = false;
                }
                else
                {
                    _datafiles = new AdminDatafile[0];
                    Datafile = null;
                    _currentItem = -1;
                    HasNext = false;
                    HasPrev = false;
                }
                OnPropertyChanged("Datafiles");
            }
        }

        private AdminDatafileArtifact[] _artifacts;
        public AdminDatafileArtifact[] Artifacts
        {
            get => _artifacts;
            set
            {
                _artifacts = value;
                OnPropertyChanged("Artifacts");
            }
        }

        private bool _hasNext = false;
        public bool HasNext
        {
            get => _hasNext;
            set
            {
                _hasNext = value;
                OnPropertyChanged("HasNext");
            }
        }
        private bool _hasPrev = false;
        public bool HasPrev
        {
            get => _hasPrev;
            set
            {
                _hasPrev = value;
                OnPropertyChanged("HasPrev");
            }
        }

        private bool _hasCurrent = false;
        public bool HasCurrent
        {
            get => _hasCurrent;
            set
            {
                _hasCurrent = value;
                OnPropertyChanged("HasCurrent");
            }
        }

        public void Next()
        {
            if (_currentItem < _datafiles.Length - 1)
            {
                _currentItem++;
                Datafile = AdminDatafileClient.GetDatafileById(_datafiles[_currentItem].Id);
                HasNext = (_currentItem < _datafiles.Length - 1);
                HasPrev = true;
                HasCurrent = true;
            }
        }

        public void Next10()
        {
            if (_currentItem < _datafiles.Length - 1)
            {
                _currentItem += 10;
                if (_currentItem >= _datafiles.Length - 1)
                {
                    _currentItem = _datafiles.Length - 1;
                }
                Datafile = AdminDatafileClient.GetDatafileById(_datafiles[_currentItem].Id);
                HasNext = (_currentItem < _datafiles.Length - 1);
                HasPrev = true;
                HasCurrent = true;
            }
        }

        public void Prev()
        {
            if (_currentItem > 0)
            {
                _currentItem--;
                Datafile = AdminDatafileClient.GetDatafileById(_datafiles[_currentItem].Id);
                HasPrev = _currentItem > 0;
                HasNext = true;
                HasCurrent = true;
            }
        }

        public void LoadData(string auditedStatus, string catalogName)
        {
            LinkedList<AdminDatafile> _datafiles = AdminDatafileClient.GetAllDatafiles(auditedStatus, catalogName);
            Datafiles = _datafiles.ToArray();
        }
    }
}

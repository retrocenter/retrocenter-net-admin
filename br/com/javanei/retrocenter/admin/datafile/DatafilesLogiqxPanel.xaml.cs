﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common.datafile;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile
{
    /// <summary>
    /// Interaction logic for DatafilesLogiqxPanel.xaml
    /// </summary>
    public partial class DatafilesLogiqxPanel : UserControl
    {
        private ObservableCollection<LogiqxDatafile> Datafiles = new ObservableCollection<LogiqxDatafile>();
        private bool Ready = false;

        public DatafilesLogiqxPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadCatalog();
            //LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                LinkedList<LogiqxDatafile> list = DatafileClient.GetAllLogiqxDatafiles();
                Datafiles.Clear();
                foreach (LogiqxDatafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }
    }
}

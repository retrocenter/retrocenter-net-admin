﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common.gamedb;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.gamedb
{
    /// <summary>
    /// Interaction logic for GameDBLBoxPanel.xaml
    /// </summary>
    public partial class GameDBLBoxPanel : UserControl
    {
        private ObservableCollection<LBoxDatafile> Datafiles = new ObservableCollection<LBoxDatafile>();
        private bool Ready = false;

        public GameDBLBoxPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadCatalog();
            //LoadPlatform();
            DataGridDatafiles.ItemsSource = Datafiles;

            Ready = true;

            Filter();
        }

        private void Filter()
        {
            if (Ready)
            {
                LinkedList<LBoxDatafile> list = GameDBClient.GetAllLBoxDatafiles();
                Datafiles.Clear();
                foreach (LBoxDatafile d in list)
                {
                    Datafiles.Add(d);
                }
            }
        }

    }
}

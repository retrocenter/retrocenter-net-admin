﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin
{
    public class Config : ApplicationSettingsBase
    {
        private const string ServerURLProperty = "ServerURL";
        private const string ServerContextPathProperty = "ServerContextPath";

        [UserScopedSetting()]
        [DefaultSettingValue("http://localhost:8080")]
        public string ServerURL
        {
            get
            {
                return ((string)this[ServerURLProperty]);
            }
            set
            {
                this[ServerURLProperty] = (string)value;
            }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("/retrocenter")]
        public string ServerContextPath
        {
            get
            {
                return ((string)this[ServerContextPathProperty]);
            }
            set
            {
                this[ServerContextPathProperty] = (string)value;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using RetrocenterAdmin.br.com.javanei.retrocenter.common.platform;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform
{
    /// <summary>
    /// Interaction logic for PlatformsPanel.xaml
    /// </summary>
    public partial class PlatformsPanel : UserControl
    {
        LinkedList<Platform> platforms;

        public PlatformsPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            platforms = PlatformClient.GetAllPlatforms();
            DataGridPlatform.ItemsSource = platforms;
        }

        private void DataGridCell_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }
    }
}

﻿using RetrocenterAdmin.br.com.javanei.retrocenter.common;
using RetrocenterAdmin.br.com.javanei.retrocenter.common.platform;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform
{
    public class PlatformClient
    {
        static Config config = new Config();

        public static RestResult<Platform> GetPlatforms(int page = 0, int pageSize = 100,
            string name = null, string alternateName = null)
        {
            HttpClient client = new HttpClient();

            try
            {
                client.BaseAddress = new Uri(config.ServerURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                RestResult<Platform> result = new RestResult<Platform>();

                string uri = config.ServerContextPath + "/api/platforms/?page="
                    + page + "&pageSize=" + pageSize;
                if (name != null && name.Length > 0)
                {
                    uri += "&name=" + HttpUtility.UrlEncode(name);
                }
                if (alternateName != null && alternateName.Length > 0)
                {
                    uri += "&alternateName=" + HttpUtility.UrlEncode(alternateName);
                }

                HttpResponseMessage response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<RestResult<Platform>>().Result;
                }

                return result;
            }
            finally
            {
                client.Dispose();
            }
        }

        public static LinkedList<Platform> GetAllPlatforms()
        {
            LinkedList<Platform> result = new LinkedList<Platform>();

            int page = 0;

            RestResult<Platform> p = GetPlatforms(page);
            while (p != null)
            {
                foreach (Platform platform in p.items)
                {
                    result.AddLast(platform);
                }
                if (p.hasNext)
                {
                    page++;
                    p = GetPlatforms(page);
                }
                else
                {
                    p = null;
                }
            }

            return result;
        }
    }
}

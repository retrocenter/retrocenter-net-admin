﻿using RetrocenterAdmin.br.com.javanei.retrocenter.admin;
using RetrocenterAdmin.br.com.javanei.retrocenter.admin.artifact;
using RetrocenterAdmin.br.com.javanei.retrocenter.admin.datafile;
using RetrocenterAdmin.br.com.javanei.retrocenter.admin.gamedb;
using RetrocenterAdmin.br.com.javanei.retrocenter.admin.platform;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;

namespace RetrocenterAdmin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static HttpClient client = new HttpClient();
        static Config config = new Config();

        private static LinkedList<ContentControl> _content = new LinkedList<ContentControl>();

        public MainWindow()
        {
            InitializeComponent();

            btClose.IsEnabled = false;
        }

        private void MenuChange(Type className)
        {
            Console.WriteLine("@@@@@ MenuChange: " + className);

            if (FContent.Children != null && FContent.Children.GetType().Equals(className))
            {
                return;
            }

            if (_content.Count > 0)
            {
                _content.Last.Value.Visibility = Visibility.Hidden;
            }
            ContentControl panel = null;
            foreach (ContentControl p in _content)
            {
                if (p.GetType().Equals(className))
                {
                    panel = p;
                }
            }
            if (panel != null)
            {
                _content.Remove(panel);
            }
            else
            {
                panel = (ContentControl)Activator.CreateInstance(className); _content.AddLast(panel);
                FContent.Children.Add(panel);
            }
            panel.Visibility = Visibility.Visible;
            btClose.IsEnabled = true;
        }

        private void MenuItemOptions_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(OptionsPanel));
        }

        private void PlatformMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(PlatformsPanel));
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemDatafileUploadDir_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafileUploadDirPanel));
        }

        private void MenuItemDatafileAll_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafilesAllPanel));
        }

        private void MenuItemDatafileCMPro_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafilesCMProPanel));
        }

        private void MenuItemDatafileLogiqx_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafilesLogiqxPanel));
        }

        private void MenuItemDatafileHyperList_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafilesHyperListPanel));
        }

        private void MenuItemDatafileMame_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(DatafilesMamePanel));
        }

        private void MenuItemGameDBLBox_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(GameDBLBoxPanel));
        }

        private void MenuItemArtifactImportHistory_Click(object sender, RoutedEventArgs e)
        {
            MenuChange(typeof(PlatformArtifactFileImportHistoryPanel));
            //ShowPanel?.Invoke(this, new ShowPanelArgs(typeof(PlatformArtifactFileImportHistoryPanel), false));
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            if (_content.Count > 0)
            {
                ContentControl panel = (ContentControl)_content.Last.Value;
                _content.Remove(panel);
                FContent.Children.Remove(panel);
                panel.Visibility = Visibility.Hidden;
                if (_content.Count > 0)
                {
                    panel = (ContentControl)_content.Last.Value;
                    panel.Visibility = Visibility.Visible;
                }
            }
            if (_content.Count == 0)
            {
                btClose.IsEnabled = false;
            }
        }

        private void MenuItemAuditDatafileClick(object sender, RoutedEventArgs e)
        {
            AuditDatafilesWindow window = new AuditDatafilesWindow();
            window.ShowDialog();
        }
    }
}
